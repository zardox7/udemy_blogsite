@extends('layouts.master')

@section('title')
    {{ $post->title }}
@endsection

@section('content')
    <article>
        @if(count($post) == 0)
            No Post
        @else
            <h1>{{ $post->title }}</h1>
            <span class="subtitle">{{ $post->author }} | {{ $post->created_at }}</span>
            <p>{{ $post->body }}</p>
        @endif
    </article>
@endsection
