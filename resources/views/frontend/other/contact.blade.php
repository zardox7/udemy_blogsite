@extends('layouts.master')

@section('title')
    Contact
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/form.css') }}">
@endsection

@section('content')
    @include('includes.info-box')
    <div class="row">
    <form action="{{ route('contact.send') }}" method="post" id="contact-form">
     <div class="input-group">
        <label for="name">Your Name</label>
        <input type="text" name="name" id="name" value="{{ Request::old('name') }}"{{ $errors->has('name') ? 'class=has-error' : '' }}>
    </div>
     <div class="input-group">
        <label for="email">Your Email</label>
        <input type="text" name="email" id="email" value="{{ Request::old('email') }}"{{ $errors->has('email') ? 'class=has-error' : '' }}>
    </div>
     <div class="input-group">
        <label for="subject">Your Subject</label>
        <input type="text" name="subject" id="subject" value="{{ Request::old('subject') }}"{{ $errors->has('subject') ? 'class=has-error' : '' }}>
    </div>
     <div class="input-group">
        <label for="message">Your Message</label>
        <textarea name="message" id="message" rows="10"{{ $errors->has('message') ? 'class=has-error' : '' }}>{{ Request::old('message') }}</textarea>
    </div>
    <button type="submit" class="btn">Submit Message</button>
    {{ csrf_field() }}
    </form>
    </div>
@endsection
