<header class="main-nav">
    <nav>
        <ul>
            <li><a href="{{ route('blog') }}">Blog</a></li>
            <li><a href="{{ route('about') }}">About</li>
            <li><a href="{{ route('contact') }}">Contact</a></li>
            <li><a href="{{ route('admin.index') }}">Admin</a></li>
        </ul>
    </nav>
</header>