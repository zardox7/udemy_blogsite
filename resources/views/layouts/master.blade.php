<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ URL::to('css/main.css') }}">
    @yield('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    @include('includes.header')
    <div class="container">
        <div class="main">
            @yield('content')
        </div>
    </div>
    @include('includes.footer')
</body>
</html>