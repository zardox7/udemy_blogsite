<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin area</title>
    <link rel="stylesheet" href="{{ URL::to('css/app.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::to('css/forms.css') }}" type="text/css">
    <style type="text/css">
        html, body, .container {
            height: 100%;
        }  
        .container {
            display: table;
            vertical-align: middle;
            padding-top: 10em;
            width: unset;
        }
        .vertical-center-row {
            display: table-cell;
            vertical-align: middle;
        }
    </style>
</head>
<body>
<div class="container">
<div>
    @include('includes.info-box')
    <form method="post" action="{{ route('admin.login') }}">
    <div class="row">
        <div class="col-md-2" style="width:10em;"> Email</div>
        <div class="col-md-4">
            <input id="email" type="text" name="email" placeholder="Email" value="{{ Request::old('email') }}" style="width:200px;"> {{ $errors->has('email') ? 'class=has-error' : '' }}
        </div>
    </div>
    <div class="row" style="padding-top: 10px;">
        <div class="col-md-2" style="width:10em;">Password</div>
        <div class="col-md-4">
            <input id="password" type="password" name="password" placeholder="Password" style="width:200px;">{{ $errors->has('password') ? 'class=has-error' : '' }}
        </div>
    </div>
    <div class="row" style="padding-top: 25px;">
        <div class="col-md-2" style="width:10em;"></div>
        <div class="col-md-4">
            <button type="submit" class="btn">Submit</button>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>
</div>
</body>
</html>