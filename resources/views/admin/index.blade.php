@extends('layouts.admin-master')

@section('styles')
    <link rel="stylesheet" href="{{ URL::to('css/modal.css') }}" type="text/css">
@endsection

@section('content')
    <div class="container">

        @include('includes.info-box')
       
        <div class="card">
            <header>
                <ul>
                    <li><a href="{{ route('admin.blog.create_post') }}" class="btn">New Post</a></li>
                    <li><a href="{{ route('admin.blog.index') }}" class="btn">Show All Posts</a></li>
                </ul>
            </header>
            <section>
                <ul>
                    @if(count($posts) == 0)
                        <li>No Posts</li>
                    @else
                        @foreach($posts as $post)
                            <li>
                                <article>
                                    <div class="post-info">
                                        <h3>{{ $post->title }}</h3>
                                        <span class="info">{{ $post->author }} | {{ $post->created_at }}</span>
                                    </div>
                                    <div class="edit">
                                        <nav>
                                            <ul>
                                                <li><a href="{{ route('admin.blog.post',['post_id' => $post->id, 'end' => 'admin']) }}">View Post</a></li>
                                                <li><a href="{{ route('admin.blog.post.edit',['post_id' => $post->id]) }}">Edit</a></li>
                                                <li><a href="{{ route('admin.blog.post.delete',['post_id' => $post->id]) }}" class="danger">Delete</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </article>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </section>
        </div>
         <div class="card">
            <header>
                <ul>
                    <li><a href="#" class="btn">Show All Messages</a></li>
                </ul>
            </header>
            <section>
                <ul>
                    @if(count($contact_messages) == 0)
                        <li>No Message</li>
                    @endif
                    @foreach($contact_messages as $message)
                        <li>
                            <article data-message="{{ $message->body }}" data-id="{{ $message->id }}" class="contact-message">
                                <div class="message-info">
                                    <h3>{{ $message->subject }}</h3>
                                    <span class="info">{{ $message->sender }} | {{ $message->created_at }}</span>
                                </div>
                                <div class="edit">
                                    <nav>
                                        <ul>
                                            <li><a href="#">View </a></li>
                                            <li><a href="#" class="danger">Delete</a></li>
                                        </ul>
                                    </nav>
                                </div>
                           </article>
                        </li>
                    @endforeach
                </ul>
            </section>
        </div>
        <div class="modal" id="contact-message-info">
            <button class="btn" id="modal-close">Close</button>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var token = "{{ Session::token() }}"
    </script>
    <script type="text/javascript" src="{{ URL::to('js/modal.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/contact_messages.js') }}"></script>
@endsection
