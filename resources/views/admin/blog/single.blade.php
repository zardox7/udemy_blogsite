@extends('layouts.admin-master')

@section('content')
    <div class="container">
        <section id="post-admin">
            <a href="{{ route('admin.blog.post.edit',['post_id' => $post->id]) }}" class="btn">Edit Post</a>
            <a href="{{ route('admin.blog.post.delete',['post_id' => $post->id]) }}" class="btn">Delete Post</a>
        </section>
        <section class="post">
            @if(count($post) == 0)
                No Post
            @else
                <h1>{{ $post->title }}</h1>
                <span class="info">{{ $post->author }} | {{ $post->created_at }}</span>
                <p class="blog">{{ $post->body }}</p>
            @endif
        </section>
    </div>
@endsection