@extends('layouts.admin-master')

@section('styles')
    <link rel="stylesheet" href="{{ URL::to('css/form.css') }}" type="text/css">
@endsection

@section('content')
    <div class="container">
        @include('includes.info-box')
        <form method="post" action="{{ route('admin.blog.category.update') }}">
            <div class="input-group">
                <label for="name">Name</label>
                <textarea name="name" id="body" rows="12" {{ $errors->has('name') ? 'class=has-error' : '' }}>{{ Request::old('body') ? Request::old('body') : isset($post->body) ? $post->body : '' }}</textarea>
            </div>
            <button type="submit" class="btn">Save Category</button>
            {{ csrf_field() }}
            <input type="hidden" name="category_id" value={{ $category->id }}>
           </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/categories.js') }}"></script>
@endsection
