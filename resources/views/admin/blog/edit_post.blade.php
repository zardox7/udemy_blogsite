@extends('layouts.admin-master')

@section('styles')
    <link rel="stylesheet" href="{{ URL::to('css/form.css') }}" type="text/css">
@endsection

@section('content')
    <div class="container">
        @include('includes.info-box')
        <form method="post" action="{{ route('admin.blog.post.update') }}">
            <div class="input-group">
                <label for="title">Title</label>
                <input id="title" type="text" name="title" placeholder="title" value="{{ 
                    Request::old('title') ? Request::old('title') : isset($post->title) ? $post->title : ''
                     }}" {{ $errors->has('title') ? 'class=has-error' : '' }}>
            </div>
            <div class="input-group">
                <label for="author">Author</label>
                <input id="author" type="text" name="author" placeholder="author" value="{{ Request::old('author') ? Request::old('author') : isset($post->author) ? $post->author : '' }}" {{ $errors->has('author') ? 'class=has-error' : '' }}>
            </div>
            <div class="input-group">
                <label for="category_select">Add Categories</label>
                <select name="category_select" id="category_select" {{ $errors->has('category_select') ? 'class=has-error' : '' }}>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
                 <button type="button" class="btn">Add Category</button>
                <div class="added-categories">
                    <ul>
                        @foreach($post_categories as $post_category)
                            <li><a href="#" data-id="{{ $post_category->id }}">{{ $post_category->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <input type="hidden" name="categories" id="categories" value="{{ implode(',',$post_categories_ids) }}">
            </div>
            <div class="input-group">
                <label for="body">Body</label>
                <textarea name="body" id="body" rows="12" {{ $errors->has('body') ? 'class=has-error' : '' }}>{{ Request::old('body') ? Request::old('body') : isset($post->body) ? $post->body : '' }}</textarea>
            </div>
            <button type="submit" class="btn">Save Post</button>
            {{ csrf_field() }}
            <input type="hidden" name="post_id" value={{ $post->id }}>
           </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/posts.js') }}"></script>
@endsection
