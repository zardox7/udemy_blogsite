<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PostController@getBlogIndex')->name('blog');
Route::get('/blog', 'PostController@getBlogIndex')->name('blog.index');
Route::get('/about', function () {
    return view('frontend.other.about');
})->name('about');
Route::get('/blog/{post_id}', 'PostController@getSinglePost')->name('blog.single');
Route::get('/contact', 'ContactMessageController@getContactIndex')->name('contact');
Route::post('/contact/sendmail', 'ContactMessageController@postSendMessage')->name('contact.send');
Route::get('/login', 'AdminController@getLogin')->name('login');
Route::post('/admin/login', 'AdminController@postLogin')->name('admin.login');

Route::group(['prefix' => '/admin','middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@getIndex')->name('admin.index');
    Route::get('/logout', 'AdminController@getLogout')->name('admin.logout');
    Route::get('/blog/posts', 'PostController@getPostIndex')->name('admin.blog.index');
    Route::get('/blog/post/{post_id}&{end}', 'PostController@getSinglePost')->name('admin.blog.post');
    Route::get('/blog/posts/create', 'PostController@getCreatePost')->name('admin.blog.create_post');
    Route::post('/blog/post/create', 'PostController@postCreatePost')->name('admin.blog.post.create');
    Route::get('/blog/post/{post_id}/edit', 'PostController@getUpdatePost')->name('admin.blog.post.edit');
    Route::post('/blog/post/update', 'PostController@postUpdatePost')->name('admin.blog.post.update');
    Route::get('/blog/post/{post_id}/delete', 'PostController@getDeletePost')->name('admin.blog.post.delete');
    Route::get('/blog/categories', 'CategoryController@getCategoryIndex')->name('admin.blog.categories');
    Route::post('/blog/category/create', 'CategoryController@postCreateCategory')->name('admin.blog.category.create');
    Route::post('/blog/categories/update', 'CategoryController@postUpdateCategory')->name('admin.blog.category.update');
    Route::get('/blog/category/{category_id}/delete', 'CategoryController@getDeleteCategory')->name('admin.blog.category.delete');
    Route::get('/contact/messages', 'ContactMessageController@getContactMessageIndex')->name('admin.contact.index');
    Route::get('/contact/message/{message_id}/delete', 'ContactMessageController@getDeleteMessage')->name('admin.contact.delete');
});
