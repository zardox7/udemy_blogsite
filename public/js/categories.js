var docReady = setInterval(function(){
    if(document.readyState !== 'complete'){
        return
    }
    clearInterval(docReady)
    var editSections = document.getElementsByClassName('edit')
    for(var i = 0; i < editSections.length; i++){
        editSections[i].firstElementChild.firstElementChild.children[1].firstChild.addEventListener('click', startEdit)
        editSections[i].firstElementChild.firstElementChild.children[2].firstChild.addEventListener('click', startDelete)
    }
    document.getElementsByClassName('btn')[0].addEventListener('click',createNewCategory)
}, 100)
function createNewCategory(event){
    event.preventDefault()
    var name = event.target.previousElementSibling.value.toLowerCase().replace(/\b\w/g,x=>x.toUpperCase())
    if(name.length === 0){
        alert('Please enter a valid category name')
        return
    }
    ajax('POST','/admin/blog/category/create','name=' + name, newCategoryCreated, [name])
}
function newCategoryCreated(params, success, responseObj){
    location.reload()
    document.getElementById('name').value = ''
}
function startEdit(event){
    event.preventDefault()
    event.target.innerText = "Save"
    var li = event.target.parentElement.previousElementSibling
    li.children[0].value = event.target.parentElement.previousElementSibling.parentElement.parentElement.parentElement.previousElementSibling.children[0].innerText
    li.style.display = "inline-block"
    li.style.maxWidth = "110px"
    setTimeout(function(){
        li.children[0].style.maxWidth = "110px";
    }, 1)
    event.target.removeEventListener('click',startEdit)
    document.getElementById('name').value = ''
    event.target.addEventListener('click',saveEdit)
}
function saveEdit(event){
    event.preventDefault()
    var li = event.target.parentElement.parentElement.children[0]
    var categoryName = li.children[0].value.toLowerCase().replace(/\b\w/g,x=>x.toUpperCase())
    if(categoryName == event.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.children[0].innerText){
        event.target.innerText="Edit"
        var li = event.target.parentElement.parentElement.children[0]
        li.style.maxWidth = "0px"
        li.style.display = "none"
        event.target.removeEventListener('click',saveEdit)
        document.getElementById('name').value = ''
        event.target.addEventListener('click',startEdit)
    }
    else{
        var categoryId = event.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.dataset['id']
        if(categoryName.length === 0){
            alert('Please enter a valid category name')
            return
        }
       ajax('POST','/admin/blog/categories/update','name='+categoryName+'&category_id='+categoryId, endEdit, [event])
    }
}
function endEdit(params, success, responseObj){
    var event = params[0]
    if(success){
        var newName = responseObj.new_name
        var article = event.target.parentElement.parentElement.parentElement.parentElement.parentElement
        article.style.backgroundColor = '#afefac'
        setTimeout(function(){
            article.style.backgroundColor = 'white'
        }, 300)
        article.firstElementChild.firstElementChild.innerHTML = newName
    }
    event.target.innerText="Edit"
    var li = event.target.parentElement.parentElement.children[0]
    li.style.maxWidth = "0px"
    setTimeout(function(){
        li.style.display = "none"
    }, 310) 
    event.target.removeEventListener('click',saveEdit)
    document.getElementById('name').value = ''
    event.target.addEventListener('click',startEdit)
}
function startDelete(event){
    deleteCategory(event)
}
function deleteCategory(event){
    event.preventDefault()
    event.target.removeEventListener('click',startDelete)
    var category_article = event.target.parentElement.parentElement.parentElement.parentElement.parentElement
   var categoryId = event.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.dataset['id']
    ajax('GET','/admin/blog/category/'+categoryId+'/delete', null, categoryDeleted, [event, category_article])
}
function categoryDeleted(params, success, responseObj){
    var article = params[1]
    if(success){
        article.style.backgroundColor = '#ffc4be'
        setTimeout(function(){
            article.remove()
            location.reload()
        }, 300)
    }
}
function ajax(method, url, params, callback, callbackParams){
    var http
    if (window.XMLHttpRequest){
        http = new XMLHttpRequest()
    }
    else{// code for IE6, IE5
        http=new ActiveXObject("Microsoft.XMLHTTP")
    }
    http.onreadystatechange=function(){
        if (http.readyState == XMLHttpRequest.DONE){
            if (http.status == 200){
                var obj = JSON.parse(http.responseText)
                callback(callbackParams, true, obj)
            }
            else if (http.status == 400){
                alert('Category could not be saved. Please try again.')
                callback(callbackParams, false)
            }
            else{
                var obj = JSON.parse(http.responseText)
                if(obj.message){
                    alert(obj.message)
                }
                else{
                    alert('ERROR. Please check the name.')
                }
            }
        }
    }
    http.open(method, baseUrl = url, true)
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
    http.setRequestHeader('X-Requested-With','XMLHttpRequest')
    http.send(params + '&_token=' + token)
}